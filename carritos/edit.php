<?php
//Include para el manejo de BD
  include '../DbSetup.php';
//Captura en una variable el id enviado del index 
  $id = $_GET['id'];
//Se valida si el request es "post", se actualiza en la BD los datos del carrito
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $nombre = $_POST['nombre'];
    $carrito_model->update($id, $nombre);
     //Pasar a la pantalla principal de carrito de compra
    return header("Location: /Shopcar");
  }
//Se llama a la tabla de carrito compra
  $carrito = $carrito_model->find($id);
?>
<!-- Muestra la interfaz de editar el carrito de compra !-->
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Editar Carrito de compras</title>
</head>
<body>
  <h3>Editar Carrito de compras</h3>
  <form method="POST">
    <label>Nombre:</label>
    <input type="text" name="nombre" required autofocus value="<?php echo $carrito['nombre']?>">
    <input type="submit" value="Salvar">
    <a href="/Shopcar">Atras</a>
  </form>
  <?php include '../shared/footer.php'; ?>
</body>
</html>
