?php
session_start();

$cart = array(
	array("product_name"=>"Producto 1","product_quantity"=>"2","product_price"=>"0.30"),
	array("product_name"=>"Producto 1","product_quantity"=>"1","product_price"=>"0.40"),
//	array("product_name"=>"Producto 1","product_quantity"=>"2","product_price"=>"0.40"),
	);

$_SESSION["cart"]=$cart;

?>
<!DOCTYPE html>
<html>
<head>
	<title>Pago</title>
<?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-md-12">

			<h3 aling="center">Productos por comprar</h3>

			<?php if(count($cart)>0):?>
			<table class="table table-striped">
				<thead>
					<th>Cantidad</th>
					<th>Producto</th>
					<th>Precio</th>
					<th>Total</th>
				</thead>
			<?php foreach($cart as $c):?>
				<tr>
					<td><?php echo $c["product_quantity"];?></td>
					<td><?php echo $c["product_name"];?></td>
					<td>$ <?php echo $c["product_price"];?></td>
					<td>$ <?php echo $c["product_quantity"]*$c["product_price"];?></td>
				</tr>
			<?php endforeach; ?>
			</table>
			<a href="./process.php" class="btn btn-primary">Proceder a Pagar</a>
			<?php endif; ?>
			<br><br>
		</div>
	</div>
</div>

</body>
</html>