<?php
 //Include de verificar la sesión
	include '../seguridad/verificar_session.php';
	$search = isset($_GET['search']) ? $_GET['search'] : '';
	//Include para el manejo de BD
	include '../DbSetup.php';
?>
<!-- Muestra la interfaz de factura y el detalle de factura !-->
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
	<title>Detalle de la Orden</title>
</head>
<body>
     <h3>Detalle de la Orden</h3>
     <table class="table table-striped">
      <tr>
        <th>Fecha de Compra</th>
        <th>Total de la Orden</th>
      </tr>
      <?php
        $result_array = $orden_model->DetalleOrden($_GET['id']); 
        if(!empty($result_array)){
        foreach ($result_array as $row) {
          echo "<tr>";
            echo "<td>" . $row['fecha_orden'] . "</td>";
            echo "<td>" ."$". $row['total'] . "</td>";
            echo "<td>" .
                  "</td>";       
          echo "</tr>";
        }
        }else{
          echo "No hay ningún orden";
        }
      ?>
    </table>
    <table class="table table-striped">
    	 <th>Productos:</th>
        <tr>
        <th>Cantidad</th>
        <th>Descripción</th>
        <th>Precio</th>
      </tr>
        <?php
        $id_orden = $_GET['id'];
        $result_array1 = $carritoxorden_model->index($id_orden); 
        if(!empty($result_array1)){
        foreach ($result_array1 as $row) {
          echo "<tr>";
            echo "<td>" . $row['cantidad_producto'] . "</td>";
            echo "<td>" . $row['descripcion_producto'] . "</td>";
            echo "<td>" ."$". $row['precio_producto'] . "</td>";
            echo "<td>" .
                  "</td>";       
          echo "</tr>";
        }
        }else{
          echo "No hay ningún producto";
        }
      ?>
    </table>
</body>
</html>