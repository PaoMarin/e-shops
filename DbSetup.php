<?php

require_once 'Db/BaseConnection.php';
require_once 'Db/PostgresConnection.php';
require_once 'Db/MySqlConnection.php';
require_once 'Models/Usuario.php';
require_once 'Models/Categoria.php';
require_once 'Models/Producto.php';
require_once 'Models/Carrito.php';
require_once 'Models/Orden.php';
require_once 'Models/CarritoxOrden.php';

/*
    Polimorfismo:
    un objeto puede tomar varias formas y funcionar diferente en cada una de sus formas.

    Ejemplo:
    el objeto $connection puede conectarse a mysql y a postgres simplemente cambiando la variable $db_class
*/

$db_class = getenv('DB_CLASS');
$connection = new $db_class(
              getenv('SERVER'),
              getenv('PORT'),
              getenv('USER'),
              getenv('PASSWORD'),
              getenv('DATABASE'));
$connection->connect();
$usuario_model = new Models\Usuario($connection);
$categoria_model = new Models\Categoria($connection);
$producto_model = new Models\Producto($connection);
$carrito_model = new Models\Carrito($connection);
$orden_model = new Models\Orden($connection);
$carritoxorden_model = new Models\CarritoxOrden($connection);

