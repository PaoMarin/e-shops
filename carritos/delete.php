<?php
    //Include de verificar la sesión
    include '../seguridad/verificar_session.php';
    //Include para el manejo de BD
    include '../DbSetup.php';
    //Captura en una variable el id enviado del index 
     $id = $_GET['id'];
     //Se valida si el request es "post", se trae de la base de datos los datos del carrito y de los productos
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
	    $carrito = $carrito_model->find($id);
	    $prod = $carrito['id_producto'];
      $producto = $producto_model->find($prod);
      //Se elimina el producto del carrito compra
      $carrito_model->deleteProducto($id);
      //Si se elimina el producto se regresa al stock de los productos
      $stock = ($producto['stock'] + $carrito['cantidad_producto']);
      //Se actualiza en la base de datos
      $producto_model->updateStock($prod,$stock);
      //Pasar a la pantalla principal de carrito de compra
	    return header("Location: /carritos/index.php");
    } 
      //Se vuelve a llamar a la tabla de carrito compra
      $car = $carrito_model->find($id);
?>
<!-- Muestra la interfaz de eliminar el carrito de compra !-->
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Eliminar Producto</title>
</head>
<body>
  <div class="container">
    <h3>Eliminar Producto</h3>
    <p>
      Esta seguro de eliminar el producto: <strong><?php echo $car['id_producto']; ?></strong>
    </p>
    <form method="POST">
      <input type="submit" value="Si">
      <a href="/carritos">No</a>
    </form>
</div>
</body>
</html>

