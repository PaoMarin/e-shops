<?php

namespace Db {
	 //Conexion a postgres
  class PostgresConnection extends BaseConnection
  {
    private $connection;
    function __construct($server, $port, $user, $password, $database)
    {
      parent::__construct($server, $port, $user, $password, $database);
    }
    //Funcion de conectar con la BD
    public function connect() {
      $connectionString = "user=$this->user password=$this->password host=$this->server port=$this->port dbname=$this->database";
      $this->connection = pg_connect($connectionString) or die('connection failed');
    }
    //Funcion de desconectar con la BD
    public function disconnect() {
      pg_close($this->connection);
    }
    //Funcion de mostrar el resultado con de la BD
    public function getResults($result) {
      return pg_fetch_all($result);
    }
    //Funcion de ejecutar la función con la BD
    public function executeSql($sql) {
      return pg_query($this->connection, $sql);
    }
  }
}
