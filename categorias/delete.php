<?php
   //Include de verificar la sesión
  include '../seguridad/verificar_session.php';
  //Include para el manejo de BD
  include '../DbSetup.php';
  //Include para el manejo de BD
  $id = $_GET['id'];
  //Se realiza un select para buscar por id en la tabla categoria
  $categoria = $categoria_model->find_for_id($id);
  //Se realiza un select para buscar productos en la tabla categoria
  $producto_categoria = $producto_model->producto_categoria($id);
  //Se valida si el request es "post", elimina una categoria 
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $categoria_model->delete($id);
    //Pasar a la pantalla principal de categorias
    return header("Location: /categorias");
  }
?>
<!-- Muestra la interfaz de eliminar una categoria !-->
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Eliminar Categorías</title>
</head>
<body>
  <div class="container">
    <h3 align="center">Eliminar Categoría</h3>
    <p>
      Descripción: <strong><?php echo $categoria['descripcion']; ?></strong>
    </p>
    <form method="POST">
     <?php
     if(empty($producto_categoria)){
      echo '<input type="submit" ' . 'value="Si">' ;
     echo '<a ' . 'href="/categorias">' . 'No' . ' </a>';
     }else{
      echo '<div class="alert info">' . 'Esta categoría tiene productos asociados, no se puede eliminar.'. '</div>';
      echo '<a ' . 'href="/categorias">' . 'Atras' . ' </a>';
     }
     ?>
      
      
    </form>
</div>
</body>
</html>

