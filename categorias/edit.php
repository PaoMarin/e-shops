<?php
  //Include de verificar la sesión
  include '../seguridad/verificar_session.php';
   //Include para el manejo de BD
  include '../DbSetup.php';
  //Captura en una variable el id enviado del index
  $id = $_GET['id'];
  //Se valida si el request es "post", se trae de la base de datos los datos de las categorias
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : '';
    $id_padre = isset($_POST['categoria']) ? $_POST['categoria'] : '';
     //Se actualiza los datos en la tabla categoria
    $categoria_model->update($id,$descripcion,$id_padre);
    //Pasar a la pantalla principal de categorias
    return header("Location: /categorias");
  }
  //Se vuelve a llamar a la tabla categoria
  $categoria = $categoria_model->find_for_id($id);
?>
<!-- Muestra la interfaz de editar la categoria !-->
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Editar Categoría</title>
</head>
<body>
  <div class="container">
    <h3>Editar Categoría</h3>
    <form method="POST">
      <table >
        <tr>
          <td>
            <label>Categoría:</label> 
          </td>
           <td><input type="text" name="descripcion" required autofocus value="<?php echo $categoria['descripcion']?>"></td>
        </tr>
        <tr>
          <td>
            <label>Categoría:</label>
          </td>
          <td>
            <?php 
            include '../DbSetup.php'; 
             $result_array = $categoria_model->find();
             echo '<select name="categoria">';
             echo '<option value="">Ninguno</option>';
            foreach ($result_array as $row) {
              echo '<option value="'.$row[id].'">'.$row[descripcion].'</option>';
            }
            echo '</select>';
            ?> 
        </tr>
        <tr><td><input type="submit" value="Salvar">
      <a href="/categorias">Atras</a></td></tr>
    </form>
 </div>

</body>
</html>
