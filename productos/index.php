<?php
//Include de verificar la sesión
include '../seguridad/verificar_session.php';
$search = isset($_GET['search']) ? $_GET['search'] : '';
?>
<!-- Muestra la interfaz principal de los productos !-->
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Página php</title>
  <meta charset="utf-8">
</head>
<body>
  <div class="container">
    <h3 align="center">Productos</h3>
    <form method="GET">
    </form>
    <table  class="table table-striped">
      <tr>
        <th>Sku</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Categoría</th>
        <th>Stock</th>
        <th>Precio</th>
        <th>Imagen</th>
        <th><a href="/productos/new.php">Nuevo</a></th>
      <?php
        include '../DbSetup.php';
        
        $result_array = $producto_model->findProductos();
        if(!empty($result_array)){
        foreach ($result_array as $row) {
          echo "<tr>";
            echo "<td>" . $row['sku'] . "</td>";
            echo "<td>" . $row['nombre'] . "</td>";
            echo "<td>" . $row['descripcion'] . "</td>";
            echo "<td>" . $row['id_categoria']. "</td>";
            echo "<td>" . $row['stock'] . "</td>";
            echo "<td>" ."$". $row['precio'] . "</td>";
            echo "<td>"  . "<img style=\"width: 18%;\"  src='/imagenes/".$row['imagen'] . "'>" ."</td>";
            echo "<td>" .
                  "<a href='/productos/edit.php?id=" . $row['id'] . "'>Editar</a>". "   " .
                  "<a href='/productos/delete.php?id=" . $row['id'] . "'>Eliminar</a>".
                  "</td>";
           
          echo "</tr>";
        }
         }else{
          echo "No hay productos disponibles";
         }
      ?>
    </table>
  </div>
</body>
</html>
