<?php

namespace Models {

  class Orden
  {
    private $connection;
    function __construct($connection)
    {
      $this->connection = $connection;
    }

    /**
     * Confirma si el nombre de la orden ya esta ingresado en la base de datos
     * @param type $nombre_orden 
     * @return type
     */
    public function confirmar($nombre_orden)
    {
      $result = $this->connection->executeSql("SELECT * FROM public.orden WHERE nombre_orden = '$nombre_orden'");
      return $this->connection->getResults($result)[0];
    }
    
    /**
     * Consulta las ordenes del usuario que esta logueado
     * @param type $search 
     * @return ordenes
     */
    public function consultaOrden($search)
    {
      $sql = "SELECT orden.id, orden.fecha_orden, orden.nombre_orden, orden.total
              FROM orden  WHERE id_usuario = " . $_SESSION["usuario_id"];
      
      $result = $this->connection->executeSql($sql);
      return $this->connection->getResults($result);
    }
   
    /**
     * Suma la cantidad de productos de la orden
     * @return sumatoria de la cantidad de productos
     */
    public function BuscarProductos()
    {
      $sql = ("SELECT SUM(cantidad_producto) FROM orden");
       $result = $this->connection->executeSql($sql);
      return $this->connection->getResults($result)[0];
    }

    /**
      * Suma la cantidad de productos de un determinado usuario.
      * @return sumatoria de la cantidad de productos
      */ 
    public function BuscarProductoCliente()
    {
      $sql = ("SELECT SUM(cantidad_producto) FROM orden WHERE id_usuario = " . $_SESSION["usuario_id"]);
       $result = $this->connection->executeSql($sql);
      return $this->connection->getResults($result)[0];
    }

    /**
     * Sumatoria del total de las compras de un determinado usuario.
     * @return sumatoria del total de las compras
     */
    public function BuscarCompras()
    {
    	$sql = ("SELECT SUM(total) FROM orden WHERE id_usuario = " . $_SESSION["usuario_id"]);
       $result = $this->connection->executeSql($sql);
      return $this->connection->getResults($result)[0];
    }
    
    /**
     * Sumatoria del total de las ordenes
     * @return total de las ordenes
     */
  	public function BuscarVentas()
    {
    	$sql = ("SELECT SUM(total) FROM orden");
       $result = $this->connection->executeSql($sql);
      return $this->connection->getResults($result)[0];
    }
    
    /**
     * Busca el la fecha y el total de una orden
     * @param int $id_orden 
     * @return fecha_orden,total
     */
    public function DetalleOrden($id_orden)
  	{
  	      $sql = "SELECT fecha_orden,total FROM orden WHERE orden.id = $id_orden";
  	      $result = $this->connection->executeSql($sql);
  	      return $this->connection->getResults($result);
  	}
    

    /**
     * Datos del detalle de la orden.
     * @param int $id_orden 
     * @return orden
     */
  	public function DetalleProducto($id_orden)
  	{
  		 $sql = "SELECT cantidad_producto, nombre_orden, total
  	         FROM  orden WHERE orden.id = $id_orden";
  	      $result = $this->connection->executeSql($sql);
  	      return $this->connection->getResults($result);
  	}

    /**
     * Crear una nueva orden
     * @param string $nombre_orden 
     * @param int $id_usuario 
     * @param int $precio 
     * @param time $fecha 
     * @param int $cantidad_producto 
     * @return void
     */
    public function insert($nombre_orden,$id_usuario,$total,$fecha_orden,$cantidad_producto)
    {
      $sql = "INSERT INTO public.orden(nombre_orden,id_usuario,total,fecha_orden,cantidad_producto) VALUES ('$nombre_orden','$id_usuario', '$total', '$fecha_orden', '$cantidad_producto')";
      $this->connection->executeSql($sql);
      
    }

      public function searchid()
    {
      $result = $this->connection->executeSql("SELECT MAX(id) FROM public.orden");
      return $this->connection->getResults($result);
    }
    
  
}
}
