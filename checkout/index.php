<?php
  //Include de verificar la sesión
  include '../seguridad/verificar_session.php';
  $search = isset($_GET['search']) ? $_GET['search'] : '';
  //Include para el manejo de BD
  include '../DbSetup.php';
  //Captura el id del usuario que esta logueado en ese momento. 
  $id_usuario= $_SESSION['usuario_id'];
   //Se valida si el request es "post", se trae de la base de datos los datos del carrito y se igualan a las variables
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    include '../DbSetup.php';      
    $result = $carrito_model->index($search);
    if(!empty($result)){
      foreach ($result as $row) {
        $cantidad_producto += $row['cantidad_producto'];
        $precio_total += $row['precio_producto'];
        $fecha = 'now()';
        $nombre_orden = isset($_POST['nombre_orden']) ? $_POST['nombre_orden'] : '';
        $confirmar_nombre_Orden = $orden_model->confirmar($nombre_orden);
       }
      if($confirmar_nombre_Orden['nombre_orden'] != $nombre_orden ){
      	//Despues de validar los datos se crea la orden
       $orden_model->insert($nombre_orden,$id_usuario,$precio_total,$fecha,$cantidad_producto);
       //Se busca el ID de la orden, el ultimo creado y se igualan las variables
       $resultado = $orden_model->searchid();
       var_dump($resultado);
       foreach ($resultado as $row) {
        $id_orden = $row["max"];
        }
       $result = $carrito_model->index($search);
       if(!empty($result)){
      foreach ($result as $row) {
        $id = $row['id'];
        $cantidad_product = $row['cantidad_producto'];
        $precio_producto = $row['precio_producto'];
        $descripcion_producto = $row['descripcion_producto'];
        //Se crea el insert del carritoxorden
        $carritoxorden_model->insert1($id_usuario,$cantidad_product,$precio_producto,$descripcion_producto,$id_orden);
       }
      }
        //Se elimina el carrio de compra de la tabla carrio_compra
         $carrito_model->delete_carrito($_SESSION['usuario_id']);
          //Pasar a la pantalla principal de la orden
          return header("Location: /consulta_orden");
        }else{
           echo "<h3>Este nombre de orden ya fue utilzado</h3>";
      }
    }
  }
  ?>
   <!-- Muestra la interfaz del checkout, el pago para crear la orden !--> 
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Página php</title>
  <meta charset="utf-8">
</head>
<body>

  <div class="container">
    <h3 align="center">Carrito de Compra</h3>
    <table class="table table-striped">
      <tr>
        <th>Producto</th>
        <th>Precio Unidad</th>
        <th>Precio total</th>
        <th>Cantidad</th>
      </tr>
      <?php
        include '../DbSetup.php';
        $result_array = $carrito_model->index($search);
         $total =0;
        if(!empty($result_array)){
        foreach ($result_array as $row) {
          $precio = ($row['cantidad_producto'] * $row['precio_producto']);
          echo "<tr>";
            echo "<td>" . $row['nombre'] . "</td>";
            echo "<td>" ."$". $row['precio_producto'] . "</td>";
            echo "<td>" ."$". $precio . "</td>";
            echo "<td>" . $row['cantidad_producto'] . "</td>";
            $total += $precio;  
          echo "</tr>";
        }
      }else{
          echo "No existen compras";
        }
         ?>
    </table>
    <?php echo "<tr>";
         echo "<td>". "<strong>". "Total a pagar: " . "<strong>" . $total . "</td>"; 
                  
          echo "</tr>";
    ?>
    <br />
    <form method="POST">
      <table class="table table-bordered">
        <tr>
          <td><label>Nombre de la orden:</label></td>
          <td><input type="text" name="nombre_orden" required autofocus></td>
        </tr>
        <tr>
          <td><label>Nombre:</label></td>
          <td><input type="text" name="nombre" required autofocus></td>
        </tr>
        <tr>
          <td><label>Numero de Tarjeta:</label></td>
          <td><input type="text" name="num_tarjeta" required autofocus></td>
        </tr>
        <tr>
          <td><label>Codigo de seguridad:</label></td>
          <td><input type="text" name="codigo" required autofocus></td>
        </tr>
      </table>
      <input  type="submit" value="Checkout">
    </form>
  </div>
</body>
</html>
