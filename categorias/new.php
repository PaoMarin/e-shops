<?php
  //Include de verificar la sesión
  include '../seguridad/verificar_session.php';
   //Se valida si el request es "post", captura los datos ingresados a las variables y las inserta en la tabla categoria
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
  	//Include para el manejo de BD
    include '../DbSetup.php';
    $descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : '';
    $id_padre = isset($_POST['categoria']) ? $_POST['categoria'] : '';
    $categoria_model->insert($descripcion,$id_padre);
    //Pasar a la pantalla principal de caegorias
    return header("Location: /categorias");
  }
?>
<!-- Muestra la interfaz de crear la cartegoría !-->
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Nuevo Categoría</title>
</head>
<body>
  <div class="container">
    <h3 align="center">Nuevo Categoría </h3>
    <form method="POST">
      <table class="table table-striped">
        <tr>
          <td>
            <label>Categoría:</label>
          </td>
          <td><input type="text" name="descripcion" required autofocus></td>
        </tr>
        <tr>
          <td>
            <label>Categoría Padre:</label>
          </td>
          <td>
            <?php 
            include '../DbSetup.php'; 
             $result_array = $categoria_model->find();
             echo '<select name="categoria">';
             echo '<option value="0">Ninguno</option>';
            foreach ($result_array as $row) {
              echo '<option value="'.$row[id].'">'.$row[descripcion].'</option>';
            }
            echo '</select>';
            ?> 
          </td>
        </tr>
        <tr><td><input type="submit" value="Guardar">
      <a href="/categorias">Atras</a></td></tr>
    </form>
</div>

</body>
</html>
