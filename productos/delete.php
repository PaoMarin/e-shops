<?php
//Include de verificar la sesión
 include '../seguridad/verificar_session.php';
  //Include para el manejo de BD
  include '../DbSetup.php';
  //Captura en una variable el id enviado del index
  $id = $_GET['id'];
  //Busca el producto
  $producto = $producto_model->find($id);
  //Se valida si el request es "post", se trae de la base de datos los datos del carrito y de los productos
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $producto_model->delete($id);
    return header("Location: /productos");
  }
?>
<!-- Muestra la interfaz de producto !-->
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Eliminar Producto</title>
</head>
<body>
  <div class="container">
    <h3>Eliminar Producto</h3>
    <p>
      Esta seguro de eliminar el producto: <strong><?php echo $producto['nombre']; ?></strong>
    </p>
    <form method="POST">
      <input type="submit" value="Si">
      <a href="/productos">No</a>
    </form>
</div>
</body>
</html>
