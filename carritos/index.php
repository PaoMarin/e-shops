<?php
//Include de verificar la sesión
include '../seguridad/verificar_session.php';
$search = isset($_GET['search']) ? $_GET['search'] : '';
//Include para el manejo de BD
include '../DbSetup.php';
?>
<!-- Muestra la interfaz de la pagina principal del carrito de compra !-->
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Página php</title>
  <meta charset="utf-8">
</head>
<body>
  <div class="container">
    <h3 align="center">Carrito de Compra</h3>
    <br>
    <a href="/checkout" class="button button1">Checkout</a>
    
    <table class="table table-striped">
      <tr>
        <th>Producto</th>
        <th>Precio Unidad</th>
        <th>Descripción</th>
        <th>Cantidad</th>
        <th>Precio Total</th>
        <th> <a href="/categorias/vista.php">Atras</a></th>
      </tr>
       <?php
        include '../DbSetup.php';
        $result_array = $carrito_model->index($search); 
        if(!empty($result_array)){
        foreach ($result_array as $row) {
          $precio = ($row['cantidad_producto'] * $row['precio_producto']);
          echo "<tr>";
            echo "<td>" . $row['nombre'] . "</td>";
            echo "<td>" ."$". $row['precio_producto'] . "</td>";
            echo "<td>" . $row['descripcion_producto'] . "</td>";
            echo "<td>" . $row['cantidad_producto'] . "</td>";
            echo "<td>" ."$". $precio . "</td>";
            echo "<td>" .
                  "<a href='/carritos/delete.php?id=" . $row['id'] . "'>Eliminar</a>".
                  "</td>"; 
            echo "</tr>";
        }
        }else{
          echo "No hay ningún producto en el carrito";
        }
      ?>
    </table>
  </div>
</body>
</html>
